const pkg = require('./package')

const title = process.env.TITLE || 'Nodes24.com - IOTA Node Pool'
const pool_url = process.env.POOL_URL || 'https://iota.nodes24.com'
const logo_url = process.env.LOGO_URL || ''


module.exports = {
  mode: 'spa',

  env: {
    title: title,
    pool_url: pool_url,
    logo_url: logo_url
  },
  /*
  ** Headers of the page
  */
  head: {
    title: title,
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: pkg.description }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      { rel: 'stylesheet', href: 'https://fonts.googleapis.com/css?family=Raleway:400,700' }
    ]
  },

  /*
  ** Customize the progress-bar color
  */
  loading: { color: '#fff' },

  /*
  ** Global CSS
  */
  css: [
  ],

  /*
  ** Plugins to load before mounting the App
  */
  router: {
    middleware: 'i18n'
  },
  plugins: [
    '~/plugins/i18n.js'
  ],

  generate: {
    routes: function () {
      let url = `${pool_url}/nodes`
      return axios.get(url)
      .then((res) => {
        return res.data.map((node) => {
          return '/nodes/' + node.name
        })
      })
    }
  },

  /*
  ** Nuxt.js modules
  */
  modules: [
    // Doc: https://github.com/nuxt-community/axios-module#usage
    '@nuxtjs/axios',
    // Doc: https://bootstrap-vue.js.org/docs/
    'bootstrap-vue/nuxt'
  ],
  /*
  ** Axios module configuration
  */
  axios: {
    // See https://github.com/nuxt-community/axios-module#options
  },

  /*
  ** Build configuration
  */
  build: {
    /*
    ** You can extend webpack config here
    */
    extend(config, ctx) {
      // Run ESLint on save
      if (ctx.isDev && ctx.isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
      }
    }
  }
}
