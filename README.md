# piri-web

> piri web interface

## Build Setup

``` bash
# install dependencies
$ yarn install

# serve with hot reload at localhost:3000
$ yarn run dev

# build for production and launch server
$ yarn run build
$ yarn start

# generate static project
$ yarn run generate
```

For detailed explanation on how things work, checkout [Nuxt.js docs](https://nuxtjs.org).


## Deployment

### Docker

### Heroku

#### ENV Variables

Go to settings and open your config variables with the button "Reveal config vars"

Add the following variables

| KEY  |  VALUE | Default |
|---|---|---|
| HOST  | 0.0.0.0 | |
| NODE_ENV | production  | |
| NPM_CONFIG_PRODUCTION  | false | |
| app_title | your pool name | piri-web interface |
| logo_url | a link to your logo | https://gitlab.com/georg.mittendorfer/piri/uploads/9b1bb2dff205bf3a8afbaad05dafa3af/piri-cut.png |
| pool_url | the url to your pool | https://pool.trytes.eu |


## Development

We use bootstrap with vue, please use these components.
[Bootstrap-Vues](https://bootstrap-vue.js.org/docs/).


#### ENV variables

Create .env file with the follow content and replace it with your data

```
app_title=einfach-iota.de
logo_url=https://einfach-iota.de/content/uploads/2018/09/einfachiota-logo.png
pool_url=https://nutzdoch.einfach-iota.de

```
