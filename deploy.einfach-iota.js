const ftp = require("basic-ftp")
const fs = require("fs")

if (process.env.NODE_ENV !== 'production') {
  console.log("hat??");
  require('dotenv').load();
  console.log(process.env.EI_FTP_HOST);

}

deploy()

async function deploy() {
    const client = new ftp.Client()
    client.ftp.verbose = true
    try {
        await client.access({
            host: process.env.EI_FTP_HOST,
            user: process.env.EI_FTP_USER,
            password: process.env.EI_FTP_PASSWORD,
            secure: false
        })
        console.log(await client.list())
        await client.ensureDir("/")
        await client.clearWorkingDir()
        await client.uploadDir("dist/")
    }
    catch(err) {
        console.log(err)
    }
    client.close()
}
