FROM node:10.12.0-slim

RUN mkdir -p /piri-web
COPY . /piri-web
WORKDIR /piri-web

COPY package.json /piri-web
COPY package-lock.json /piri-web
RUN npm install

ENV NODE_ENV=production

COPY . /piri-web
RUN npm run build

ENV HOST 0.0.0.0
EXPOSE 3000
CMD ["npm", "start"]
